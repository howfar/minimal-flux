import test from 'tape'
import Dispatcher from './../src/Dispatcher'
import Actions from './../src/Actions'
import Store from './../src/Store'

test('Dispatcher: dispatch', (t) => {
  let handled = []

  class FooActions extends Actions {
    foo () { this.dispatch('foo') }
  }

  class FooStore extends Store {
    constructor () {
      super()
      this.handleAction('foo.foo', this.handleFooFoo)
    }

    handleFooFoo () { handled.push('foo') }
  }
  class BarStore extends Store {
    constructor () {
      super()
      this.handleAction('foo.foo', this.handleFooFoo)
    }

    handleFooFoo () { handled.push('bar') }
  }
  class BazStore extends Store {
    constructor () {
      super()
      this.handleAction('foo.foo', this.handleFooFoo)
    }

    handleFooFoo () { handled.push('baz') }
  }

  let flux = new Dispatcher({
    actions: {
      foo: FooActions
    },
    stores: {
      foo: FooStore,
      bar: [ BarStore, 'foo', 'baz' ],
      baz: BazStore
    }
  })

  flux.actions.foo.foo()
  t.deepEqual(handled, [ 'baz', 'foo', 'bar' ],
    'should dispatch action in a topological order')

  t.end()
})

test('Dispatcher: dispatch while dispatching', (t) => {
  let flux

  class FooStore extends Store {
    constructor () {
      super()
      this.state = {
        foo: false,
        bar: false
      }

      this.handleAction('foo.foo', () => {
        this.setState({ foo: true })
        flux.actions.foo.bar()
      })

      this.handleAction('foo.bar', () => {
        this.setState({ bar: true })
        flux.actions.foo.assert()
      })

      this.handleAction('foo.assert', () => {
        const { foo, bar } = this.getState()
        t.ok(foo && bar)
        t.end()
      })
    }
  }

  class FooActions extends Actions {
    foo () { this.dispatch('foo') }

    bar () { this.dispatch('bar') }

    assert () { this.dispatch('assert') }
  }

  flux = new Dispatcher({
    stores: { foo: FooStore },
    actions: { foo: FooActions }
  })
  flux.actions.foo.foo()
})
