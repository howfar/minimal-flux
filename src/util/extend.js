'use strict'

export default function extend (SuperClass, properties) {
  function SubClass () {
    SuperClass.call(this)
  }

  SubClass.prototype = Object.create(SuperClass.prototype)
  Object.assign(SubClass.prototype, { constructor: SuperClass }, properties)
  return SubClass
}
