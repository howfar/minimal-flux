/// <reference path="../node_modules/eventemitter3/index.d.ts" />
declare module 'minimal-flux-2' {
  import EventEmitter3 = require('eventemitter3');

  interface ActionsDictionary {
    [name: string]: Actions;
  }

  interface StoresDictionary<T> {
    [name: string]: Store<T>;
  }

  interface MinimalFluxConfig {
    actions: Object;
    stores: Object;
  }

  interface SetStateOptions {
    silent?: boolean;
  }

  export class Actions extends EventEmitter3 {
    protected dispatch(id: string, ...payload: any[]);
  }

  export class Store<T extends Object> extends EventEmitter3 {
    protected state: T;

    protected handleAction(id: string, handler: Function): void;
    protected stopHandleAction(id: string);
    protected setState(newState?: T, options?: SetStateOptions): void;

    public getState(): T;
  }

  export class Flux extends EventEmitter3 {
    constructor(config: MinimalFluxConfig);
    actions: ActionsDictionary;
    stores: StoresDictionary<Object>;
  }
}

declare module 'minimal-flux' {
  import alias = require('minimal-flux-2');
  export default alias;
}
