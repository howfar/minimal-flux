import EventEmitter from 'eventemitter3'
import Dispatcher from './Dispatcher'

export default class Flux extends EventEmitter {

  /**
   * Constructor
   * @param  {Object} options
   * @param  {Object} options.actions Namespaced actions
   * @param  {Object} options.stores  Namespaced stores
   * @return {Flux}
   */
  constructor (options) {
    super()

    const dispatcher = new Dispatcher(options)

    // Forward dispatcher events
    dispatcher.addListener('error', this.emit.bind(this, 'error'))
    dispatcher.addListener('dispatch', this.emit.bind(this, 'dispatch'))
    dispatcher.addListener('done', this.emit.bind(this, 'done'))

    this.actions = dispatcher.actions
    this.stores = dispatcher.stores
  }
}
