'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _eventemitter3 = require('eventemitter3');

var _eventemitter32 = _interopRequireDefault(_eventemitter3);

var _Dispatcher = require('./Dispatcher');

var _Dispatcher2 = _interopRequireDefault(_Dispatcher);

var Flux = (function (_EventEmitter) {
  _inherits(Flux, _EventEmitter);

  /**
   * Constructor
   * @param  {Object} options
   * @param  {Object} options.actions Namespaced actions
   * @param  {Object} options.stores  Namespaced stores
   * @return {Flux}
   */

  function Flux(options) {
    _classCallCheck(this, Flux);

    _get(Object.getPrototypeOf(Flux.prototype), 'constructor', this).call(this);

    var dispatcher = new _Dispatcher2['default'](options);

    // Forward dispatcher events
    dispatcher.addListener('error', this.emit.bind(this, 'error'));
    dispatcher.addListener('dispatch', this.emit.bind(this, 'dispatch'));
    dispatcher.addListener('done', this.emit.bind(this, 'done'));

    this.actions = dispatcher.actions;
    this.stores = dispatcher.stores;
  }

  return Flux;
})(_eventemitter32['default']);

exports['default'] = Flux;
module.exports = exports['default'];