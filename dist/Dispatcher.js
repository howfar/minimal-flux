'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _slicedToArray = (function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i['return']) _i['return'](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError('Invalid attempt to destructure non-iterable instance'); } }; })();

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i]; return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _eventemitter3 = require('eventemitter3');

var _eventemitter32 = _interopRequireDefault(_eventemitter3);

var _toposort = require('toposort');

var _toposort2 = _interopRequireDefault(_toposort);

var _getallpropertynames = require('getallpropertynames');

var _getallpropertynames2 = _interopRequireDefault(_getallpropertynames);

var _utilExtend = require('./util/extend');

var _utilExtend2 = _interopRequireDefault(_utilExtend);

var _utilHasPrefix = require('./util/hasPrefix');

var _utilHasPrefix2 = _interopRequireDefault(_utilHasPrefix);

var _Actions = require('./Actions');

var _Actions2 = _interopRequireDefault(_Actions);

var allObjectProperties = (0, _getallpropertynames2['default'])({});
var allActionsProperties = (0, _getallpropertynames2['default'])(_Actions2['default'].prototype);
var eventEmitterProperties = Object.keys(_eventemitter32['default'].prototype);

var Dispatcher = (function (_EventEmitter) {
  _inherits(Dispatcher, _EventEmitter);

  /**
   * Constructor
   * @param  {Object} options
   * @param  {Object} options.actions Namespaced actions
   * @param  {Object} options.stores  Namespaced stores
   * @return {Dispatcher}
   */

  function Dispatcher(options) {
    _classCallCheck(this, Dispatcher);

    _get(Object.getPrototypeOf(Dispatcher.prototype), 'constructor', this).call(this);
    // Wrapped actions
    this.actions = {};
    this.actionIds = [];
    // Wrapped stores
    this.stores = {};
    // Actual stores
    this._stores = {};
    // Order in which actions get dispatched to the stores
    this.order = [];

    var actions = options.actions || {};
    var stores = options.stores || {};

    this.createActions(actions);
    this.createStores(stores);

    this._queue = [];
  }

  /**
   * Dispatches an action
   * @param  {String}    id   The id of the action (e.g. 'todos.create')
   * @param  {...mixed}  args Arguments that will be passed to the handlers
   * @return {void}
   */

  _createClass(Dispatcher, [{
    key: 'dispatch',
    value: function dispatch(id) {
      var _this = this;

      for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        args[_key - 1] = arguments[_key];
      }

      if (this._isDispatching) {
        this._queue.push([id].concat(_toConsumableArray(args)));
        if (process.env.NODE_ENV !== 'production') {
          console.warn('Action "' + id + '" was queued for later execution');
        }
        return;
      }

      this._isDispatching = true;

      this.emit.apply(this, ['dispatch', id].concat(_toConsumableArray(args)));

      // Run through stores and invoke registered handlers
      var stores = this._stores;
      for (var i = 0; i < this.order.length; i++) {
        var key = this.order[i];
        var handlers = stores[key]._handlers;
        if (!handlers || !handlers[id]) continue;

        try {
          handlers[id].apply(handlers, _toConsumableArray(args));
        } catch (err) {
          if (process.env.NODE_ENV !== 'production') {
            console.error(err.stack || err);
          }
          this.emit('error', err);
          break;
        }
      }

      this._isDispatching = false;

      this.emit('done');

      // Check and process queue if not empty
      var queue = this._queue;
      if (queue.length > 0) {
        var _queue$splice = queue.splice(0, 1);

        var _queue$splice2 = _slicedToArray(_queue$splice, 1);

        args = _queue$splice2[0];

        setTimeout(function () {
          _this.dispatch.apply(_this, _toConsumableArray(args));
        }, 1);
      }
    }

    /**
     * Create actions
     * @param  {Object} actions Namespaced actions
     * @return {void}
     */
  }, {
    key: 'createActions',
    value: function createActions(actions) {
      var _this2 = this;

      var _loop = function (key) {
        var Actions = actions[key];
        // Make actions available at construction time
        var ExtendedActions = (0, _utilExtend2['default'])(Actions, {
          key: key,
          actions: _this2.actions,
          stores: _this2.stores
        });

        // Instantiate actions
        var instance = new ExtendedActions();
        // Create wrapped actions object
        _this2.actions[key] = {};
        // Find actual action function
        var props = (0, _getallpropertynames2['default'])(Actions.prototype).filter(function (prop) {
          // Ignore the base class properties
          return allActionsProperties.indexOf(prop) < 0 &&
          // Only regard functions
          typeof instance[prop] === 'function';
        });
        // Run through actual actions
        for (var i = 0; i < props.length; i++) {
          var prop = props[i];
          // Bind function to instance
          var fn = instance[prop] = instance[prop].bind(instance);
          // The action id is composed from the actions key and its function name
          var id = [key, prop].join('.');
          _this2.actionIds.push(id);
          // Listen to the action event
          instance.addListener(prop, _this2.dispatch.bind(_this2, id));
          // Add function to the wrapped object
          _this2.actions[key][prop] = fn;
        }
      };

      // Run through namespaced actions
      for (var key in actions) {
        _loop(key);
      }
    }

    /**
     * Check if action id exists
     * @param  {String} id The action id
     * @return {Boolean}
     */
  }, {
    key: 'actionIdExists',
    value: function actionIdExists(id) {
      return this.actionIds.indexOf(id) > -1;
    }

    /**
     * Create stores
     * @param  {Object} stores Namespaced stores
     * @return {void}
     */
  }, {
    key: 'createStores',
    value: function createStores(stores) {
      var _this3 = this;

      var nodes = [];
      var edges = [];

      // Create a dependency graph
      for (var key in stores) {
        nodes.push(key);
        var store = stores[key];
        // If store is not an array, it has no dependencies
        if (!Array.isArray(store)) continue;
        var deps = store.slice(1) || [];
        // Add edges between store and it dependencies
        for (var i = 0; i < deps.length; i++) {
          edges.push([key, deps[i]]);
        }
      }

      // Topological sort, store the order
      var order = this.order = _toposort2['default'].array(nodes, edges).reverse();

      // Run through ordered stores

      var _loop2 = function (i, l) {
        var key = order[i];
        var Store = stores[key];
        // Handle plain and array definition
        if (Array.isArray(Store)) Store = Store[0];
        // Make stores available at construction time
        var ExtendedStore = (0, _utilExtend2['default'])(Store, {
          key: key,
          stores: _this3.stores,
          _actionIdExists: _this3.actionIdExists.bind(_this3)
        });
        // Instantiate the store
        var instance = new ExtendedStore();
        _this3._stores[key] = instance;
        // Create a wrapped stores object
        _this3.stores[key] = {};
        // Find functions that will be added to the wrapped object
        var props = (0, _getallpropertynames2['default'])(Store.prototype).filter(function (prop) {
          // Only regard functions
          return typeof instance[prop] === 'function' && allObjectProperties.indexOf(prop) < 0 && (
          // Functions that start with get or is
          (0, _utilHasPrefix2['default'])(prop, 'get') || (0, _utilHasPrefix2['default'])(prop, 'is') || (0, _utilHasPrefix2['default'])(prop, 'has') ||
          // Event emitter function, except emit
          eventEmitterProperties.indexOf(prop) > -1 && prop !== 'emit');
        });
        // Run through functions
        for (var _i = 0; _i < props.length; _i++) {
          var prop = props[_i];
          // Bind function to the instance and add it to the wrapped object
          _this3.stores[key][prop] = instance[prop].bind(instance);
        }
      };

      for (var i = 0, l = order.length; i < l; i++) {
        _loop2(i, l);
      }
    }
  }]);

  return Dispatcher;
})(_eventemitter32['default']);

exports['default'] = Dispatcher;
module.exports = exports['default'];