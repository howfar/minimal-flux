'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports['default'] = extend;

function extend(SuperClass, properties) {
  function SubClass() {
    SuperClass.call(this);
  }

  SubClass.prototype = Object.create(SuperClass.prototype);
  Object.assign(SubClass.prototype, { constructor: SuperClass }, properties);
  return SubClass;
}

module.exports = exports['default'];