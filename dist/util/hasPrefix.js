"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = hasPrefix;
function isCapital(letter) {
  return letter === letter.toUpperCase();
}

function hasPrefix(str, prefix) {
  return str.indexOf(prefix) === 0 && (str.length === prefix.length || isCapital(str[prefix.length]));
}

module.exports = exports["default"];