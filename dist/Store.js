'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x2, _x3, _x4) { var _again = true; _function: while (_again) { var object = _x2, property = _x3, receiver = _x4; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x2 = parent; _x3 = property; _x4 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _eventemitter3 = require('eventemitter3');

var _eventemitter32 = _interopRequireDefault(_eventemitter3);

var Store = (function (_EventEmitter) {
  _inherits(Store, _EventEmitter);

  function Store() {
    _classCallCheck(this, Store);

    _get(Object.getPrototypeOf(Store.prototype), 'constructor', this).apply(this, arguments);
  }

  _createClass(Store, [{
    key: 'handleAction',

    /**
     * Register an action handler
     * @param  {String}   id      Id of the action (e.g. 'todos.create')
     * @param  {Function} handler Action handler
     * @return {void}
     */
    value: function handleAction(id, handler) {
      if (!this._handlers) this._handlers = {};

      if (typeof handler !== 'function') {
        throw new Error('Handler for action ' + id + ' is not a function. ' + ('Attempted to register action handler in ' + this.constructor.name + '.'));
      }
      if (!this._actionIdExists(id)) {
        throw new Error('Action ' + id + ' does not exist. ' + ('Attempted to register action handler in ' + this.constructor.name + '.'));
      }
      if (this._handlers[id]) {
        throw new Error('Handler for action ' + id + ' is already registered. ' + ('Attempted to register action handler in ' + this.constructor.name + '.'));
      }

      this._handlers[id] = handler.bind(this);
    }

    /**
     * Unregister an action handler
     * @param  {String} id  Id of the action (e.g. 'todos.create')
     * @return {void}
     */
  }, {
    key: 'stopHandleAction',
    value: function stopHandleAction(id) {
      if (!id || !this._handlers || !this._handlers[id]) return;
      this._handlers[id] = undefined;
    }

    /**
     * Set state
     * @param {Object} state State object
     * @param {Object} options
     * @param {boolean} options.silent
     */
  }, {
    key: 'setState',
    value: function setState(state) {
      var options = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

      if (!state) return;
      if (!this.state) this.state = {};
      this.state = Object.assign({}, this.state, state);

      if (options.silent !== true) {
        this.emit('change', this.state);
      }
    }

    /**
     * Returns the state
     * @return {Object} The state object
     */
  }, {
    key: 'getState',
    value: function getState() {
      return this.state || {};
    }

    /**
     * Check if action id exists
     * @return {Boolean}
     */
  }, {
    key: '_actionIdExists',
    value: function _actionIdExists(id) {
      // This method will be overriden by the dispatcher.
      // When testing stores without the dispatcher, always return true
      return true;
    }
  }]);

  return Store;
})(_eventemitter32['default']);

exports['default'] = Store;
module.exports = exports['default'];