# minimal-flux-2

A fork of [minimal-flux](https://github.com/malte-wessel/minimal-flux) version [0.4.11](https://github.com/malte-wessel/minimal-flux/tree/8a3d47701689997fd4f1d683b109028ad9819a48)

## Install

````bash
npm install minimal-flux-2 --save
````

## Changes

### 1.0.2

* Fixed paths to typescript definitions

### 1.0.1

* Updated development error logging to log error's stack trace
* Added `esnext:main` entry to `package.json`

### 1.0.0

* Actions, dispatched during an already running dispatch are queued and executed after dispatch is done
* If an error is thrown during dispatch, dispatch is interrupted, yet other actions can be dispatched
* `done` event emitted when a dispatch is finished without errors

## License
MIT

[![JavaScript Style Guide](https://cdn.rawgit.com/feross/standard/master/badge.svg)](https://github.com/feross/standard)
